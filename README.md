# vpd-suocedex

Structure derivation from veekun's pokedex model, with a simpler data model and interface more worldbuilding and less game oriented. 

Used primarily for keeping track of adaptations to canon as well as of fakemon developments.

